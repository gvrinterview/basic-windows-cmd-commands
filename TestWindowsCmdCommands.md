# Questions

1. What command clears the contents of your terminal display?

- CLS
- CLEAR
- DELETE
- REMOVE

2. What is the command to change directories ?

- DIR
- CD
- BAT
- CMD

3. What is the command to list directory content ?

- LIST
- DIR
- FILES_AND_FOLDER
- DIRECTORY

4. What is the command to retrieve IP address ?

- IPGET
- GETIP
- IPCONFIG
- CONFIGIP

5. What is the command to find display TCP/IP connections and status ?

- NETSH
- NETSTAT
- TCPSTATUS
- STATUS_TCP

6. What is the command to create a folder ?

- CREATE
- FOLDERNEW
- MKDIR
- DIRMK

7. What is the command to do a hard disk check ?

- SCANDISK
- DISKCHECK
- CHKDSK
- DSKCHECKHARD

8. What is the command to rename a file ?

- RENAME
- REPLACE
- RECALL
- NAMECHANGE

9. What is the correct syntax for moving multiple files from one path to another ?

- moved source_path destination_path
- move source_path destination_path
- remove source_path destination_path
- copy source_path destination_path

10. What is the command to delete a directory ?

- DEL
- DELETE DIR
- DELETEDIR / DIRCANCELLED
- RMDIR / RD